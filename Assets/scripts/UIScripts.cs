﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScripts : MonoBehaviour {
    public void Logout() {
        PlayerPrefs.DeleteAll();
        API.userObject = null;
        Application.LoadLevel("login");
    }
    public void goTo(string level){
        Application.LoadLevel(level);
    }
}
