﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class BtnsScript : MonoBehaviour {
    public VideoPlayer vid;
    public Transform play;
    public static bool isPlayed=false;
	// Use this for initialization
	void Start () {
        vid = GetComponent<VideoPlayer>();
	}

    public void PlayPauseToggle(bool playPause) {

        if (isPlayed) {
            playPause = !playPause;
        }
        if (playPause)
        {
            playPause = !isPlayed;
            play.gameObject.SetActive(false);
            vid.Play();
        }
        else {
            play.gameObject.SetActive(true);
            playPause = isPlayed;
            vid.Pause();
        }
    }
    public void PlayVid() {
        play.gameObject.SetActive(false);
        vid.Play();
        isPlayed = true;
    }
}
