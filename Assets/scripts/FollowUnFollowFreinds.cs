﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class FollowUnFollowFreinds : MonoBehaviour {
    public GameObject loadingIndicator;
    string API_LINK = "https://vapp.m-apps.co/api/v1/conversations/people/follow/en";

    public void FollowFreinds() {
        bool isFollowed = transform.GetChild(2).GetComponent<Toggle>().isOn;
        StartCoroutine(FollowUnfollow(isFollowed));
    }
    IEnumerator FollowUnfollow(bool isFollowed)
    {
        loadingIndicator.SetActive(true);
        WWWForm form = new WWWForm();
        //form.AddField("from_id", PlayerPrefs.GetString("user_id"));
        form.AddField("from_id","1");
        form.AddField("to_id", name);

        using (UnityWebRequest www = UnityWebRequest.Post(API_LINK, form))
        {
            yield return www.SendWebRequest();

            loadingIndicator.SetActive(false);

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);


            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                loadingIndicator.SetActive(false);              
            }
        }
    }
}
