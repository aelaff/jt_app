﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class LikeDislike : MonoBehaviour {
    string APILink = "https://vapp.m-apps.co/api/v1/";

    public void Like()
    {
        //StartCoroutine(ToggleLike(int.Parse(name),PlayerPrefs.GetInt("user_id")));
        StartCoroutine(ToggleLike(int.Parse(name), 1));
    }
    IEnumerator ToggleLike(int comment_id, int user_id)
    {

        WWWForm form = new WWWForm();
        form.AddField("comment_id", comment_id);
        form.AddField("user_id", user_id);

        using (UnityWebRequest www = UnityWebRequest.Post(APILink + "channels/video/comment/like/en", form))
        {
            yield return www.SendWebRequest();


            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                //listener.OnFailed(callerId);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);

            }
        }
    }
}
