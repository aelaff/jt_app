﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class API : MonoBehaviour {
    string APILink = "https://vapp.m-apps.co/api/v1/auth/";
    public InputField username , password;
    public InputField username2 , password2,fullname2;
    public static UserObject userObject;
    public Transform popup;
    public GameObject loadingIndicator;

    private void Start()
    {
        if (PlayerPrefs.HasKey("user_id")) {
            StartCoroutine(LoginUser(PlayerPrefs.GetString("username"), PlayerPrefs.GetString("password")));
        }
    }
    public void Login()
    {
        StartCoroutine(LoginUser(username.text, password.text));
    }
    public void Register()
    {
        StartCoroutine(RegisterUser(fullname2.text,username2.text, password2.text));
    }

    IEnumerator LoginUser( string username, string password)
    {
        loadingIndicator.SetActive(true);

        PlayerPrefs.SetString("username", username);
        PlayerPrefs.SetString("password", password);
        WWWForm form = new WWWForm();
        form.AddField("email", username);
        form.AddField("password", password);

        using (UnityWebRequest www = UnityWebRequest.Post(APILink + "Login/en", form))
        {
            yield return www.SendWebRequest();

            loadingIndicator.SetActive(false);

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                popup.gameObject.SetActive(true);
                popup.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = "Error";
                popup.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = www.error;

            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                 userObject = JsonUtility.FromJson<UserObject>(www.downloadHandler.text);
                Debug.Log(userObject.message);
                if (userObject.status)
                {
                    popup.gameObject.SetActive(true);
                    popup.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = "Login";
                    popup.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = userObject.message;

                    Application.LoadLevel("Main");
                    PlayerPrefs.SetInt("user_id", userObject.data.id);
                }
                else {
                    popup.gameObject.SetActive(true);
                    popup.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = "Login";
                    popup.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = userObject.message;

                }
                // listener.OnCompleted(www.downloadHandler.text, callerId);
            }
        }
    }
    IEnumerator RegisterUser(string full_name, string username, string password)
    {
        loadingIndicator.SetActive(true);

        WWWForm form = new WWWForm();
        form.AddField("email", username);
        form.AddField("password", password);
        form.AddField("full_name", full_name);

        using (UnityWebRequest www = UnityWebRequest.Post(APILink + "Register/en", form))
        {
            yield return www.SendWebRequest();

            loadingIndicator.SetActive(false);

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                popup.gameObject.SetActive(true);
                popup.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = "Error";
                popup.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = www.error;
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                userObject = JsonUtility.FromJson<UserObject>(www.downloadHandler.text);
                Debug.Log(userObject.message);
                if (userObject.status)
                {
                    popup.gameObject.SetActive(true);
                    popup.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = "Login";
                    popup.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = userObject.message;
                    Application.LoadLevel("Main");
                    PlayerPrefs.SetInt("user_id", userObject.data.id);


                }
                else {
                    popup.gameObject.SetActive(true);
                    popup.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = "Login";
                    popup.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = userObject.message;
                }
            }
        }
    }



    [System.Serializable]
    public class User
    {
        public int id;
        public string full_name;
        public string email;
        public string profile_image;
        public string ip;
        public string fcm_token;
        public string device_type;
        public bool status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public List<Video> channel;

    }
    [System.Serializable]
    public class UserObject
    {
        public bool status;
        public string message;
        public User data;
    }
    [System.Serializable]
    public class Video
    {
        public int id;
        public int user_id;
        public int category_id;
        public string title;
        public string description;
        public string image;
        public string video;
        public int views_count;
        public string status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public Category category;

    }
    [System.Serializable]
    public class Category
    {
        public int id;
        public string title;
        public string updated_at;
        public string created_at;
        public string deleted_at;
    }
}
