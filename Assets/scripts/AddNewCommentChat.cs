﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class AddNewCommentChat : MonoBehaviour {
    string APILink = "https://vapp.m-apps.co/api/v1/";
    public Transform commentPrefab;
    public Transform CommentsParent;
    public InputField commentText;
    public GameObject loadingIndicator;
    public void AddNew () {
        //StartCoroutine(GetVideoComments(PlayerPrefs.GetInt("video_id"),PlayerPrefs.GetInt("user_id"), commentText.text));
        StartCoroutine(SendMessage(1, PlayerPrefs.GetInt("FromUser"), PlayerPrefs.GetInt("ConvId"), commentText.text));
	}
    public void EnableDisable(string s) {
        if (s == null || s == "")
        {
            GetComponent<Button>().interactable = false;
        }
        else {
            GetComponent<Button>().interactable = true;
        }
    }
    IEnumerator SendMessage(int from_user_id, int to_user_id, int conversation_id, string message)
    {
        loadingIndicator.SetActive(true);

        WWWForm form = new WWWForm();
        form.AddField("from_user_id", from_user_id);
        form.AddField("to_user_id", to_user_id);
        form.AddField("conversation_id", conversation_id);
        form.AddField("message", message);

        using (UnityWebRequest www = UnityWebRequest.Post(APILink + "conversations/message/new/ar", form))
        {
            yield return www.SendWebRequest();


            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                //listener.OnFailed(callerId);
                loadingIndicator.SetActive(false);

            }
            else
            {
                loadingIndicator.SetActive(false);

                Debug.Log(www.downloadHandler.text);
                
                    
                    Transform comment2 = Instantiate(commentPrefab);
                    comment2.GetChild(0).GetChild(0).GetComponent<Text>().text = commentText.text;
                    
                   
                    comment2.SetParent(CommentsParent);
                    commentText.text = "";
                
            }
        }
    }
    
    [System.Serializable]
    public class VideoObject
    {
        public bool status;
        public string message;
        public Video data;
    }
    [System.Serializable]
    public class Video
    {
        public int id;
        public int user_id;
        public int category_id;
        public string title;
        public string description;
        public string image;
        public string video;
        public int views_count;
        public string status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public int likes_count;
        public Category category;
        public List<Comments> comments;

    }
    [System.Serializable]
    public class Category
    {
        public int id;
        public string title;
        public string updated_at;
        public string created_at;
        public string deleted_at;
    }
    [System.Serializable]
    public class Comments
    {
        public int id;
        public string user_id;
        public string video_id;
        public string comment;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public int liked;
        public string profile_image;
        public string full_name;
    }
   

}
