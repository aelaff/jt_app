﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileData : MonoBehaviour {
    public Text full_name;
    public Text email;
    public Image profile_image;
    public GameObject loadingIndicator;

    private void Awake()
    {
        loadingIndicator.SetActive(true);

        full_name.text = API.userObject.data.full_name;
        email.text = API.userObject.data.email;
        StartCoroutine(downloadImage());
    
}
    IEnumerator downloadImage()
    {
        // Start a download of the given URL
        using (WWW www = new WWW(API.userObject.data.profile_image))
        {
            // Wait for download to complete
            yield return www;
            loadingIndicator.SetActive(false);

            profile_image.sprite= Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
    }
}
