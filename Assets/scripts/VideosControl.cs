﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideosControl : MonoBehaviour {
    public List<VideoPlayer> videos;
    public Transform videosPanel;
    public GameObject videoPrefab;
    public GameObject loadingIndicator;

    public void AddNewVid(string videoName)
    {
        loadingIndicator.SetActive(true);

        var video = Resources.Load<VideoClip>("Videos/"+ videoName);
        GameObject newVid = Instantiate(videoPrefab);
        newVid.GetComponent<VideoPlayer>().clip = video;
        newVid.transform.SetParent(videosPanel);
        loadingIndicator.SetActive(false);

    }

}
