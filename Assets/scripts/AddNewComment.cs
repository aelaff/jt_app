﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class AddNewComment : MonoBehaviour {
    string APILink = "https://vapp.m-apps.co/api/v1/";
    public Transform commentPrefab;
    public Transform CommentsParent;
    public InputField commentText;
    public GameObject loadingIndicator;
    public void AddNew () {
        StartCoroutine(GetVideoComments(PlayerPrefs.GetInt("video_id"),PlayerPrefs.GetInt("user_id"), commentText.text));
        //StartCoroutine(GetVideoComments(1, 1, commentText.text));
	}
    public void EnableDisable(string s) {
        if (s == null || s == "")
        {
            GetComponent<Button>().interactable = false;
        }
        else {
            GetComponent<Button>().interactable = true;
        }
    }
    IEnumerator GetVideoComments(int video_id, int user_id,string comment)
    {
        loadingIndicator.SetActive(true);

        WWWForm form = new WWWForm();
        form.AddField("video_id", video_id);
        form.AddField("user_id", user_id);
        form.AddField("comment", comment);

        using (UnityWebRequest www = UnityWebRequest.Post(APILink + "channels/video/comment/new/en", form))
        {
            yield return www.SendWebRequest();


            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                //listener.OnFailed(callerId);
                loadingIndicator.SetActive(false);

            }
            else
            {
                loadingIndicator.SetActive(false);

                Debug.Log(www.downloadHandler.text);
                VideoObject videoObject = JsonUtility.FromJson<VideoObject>(www.downloadHandler.text);
                Debug.Log(videoObject.message);
                if (videoObject.status)
                {
                    
                    Transform comment2 = Instantiate(commentPrefab);
                    comment2.GetChild(0).GetComponent<Text>().text = videoObject.data.comments[videoObject.data.comments.Count-1].full_name;
                    //Debug.Log(c.user);
                    comment2.GetChild(2).GetComponent<Text>().text = videoObject.data.comments[videoObject.data.comments.Count - 1].comment;
                    comment2.GetChild(3).name = videoObject.data.comments[videoObject.data.comments.Count - 1].id + "";
                    comment2.GetChild(3).GetComponent<Toggle>().isOn = false;
                   
                    StartCoroutine(downloadImage(comment2.GetChild(1).GetChild(0).GetComponent<Image>(), videoObject.data.comments[videoObject.data.comments.Count - 1].profile_image));
                    comment2.SetParent(CommentsParent);
                    commentText.text = "";
                }
                else
                {

                }
            }
        }
    }
    IEnumerator downloadImage(Image m, string linkk)
    {
        loadingIndicator.SetActive(true);

        // Start a download of the given URL
        using (WWW www = new WWW(linkk))
        {

            // Wait for download to complete
            yield return www;
            loadingIndicator.SetActive(false);

            m.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
            CommentsParent.parent.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 0);

        }
    }
    [System.Serializable]
    public class VideoObject
    {
        public bool status;
        public string message;
        public Video data;
    }
    [System.Serializable]
    public class Video
    {
        public int id;
        public int user_id;
        public int category_id;
        public string title;
        public string description;
        public string image;
        public string video;
        public int views_count;
        public string status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public int likes_count;
        public Category category;
        public List<Comments> comments;

    }
    [System.Serializable]
    public class Category
    {
        public int id;
        public string title;
        public string updated_at;
        public string created_at;
        public string deleted_at;
    }
    [System.Serializable]
    public class Comments
    {
        public int id;
        public string user_id;
        public string video_id;
        public string comment;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public int liked;
        public string profile_image;
        public string full_name;
    }
   

}
