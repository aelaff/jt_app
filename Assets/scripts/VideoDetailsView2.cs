﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoDetailsView2 : MonoBehaviour {
    string APILink = "https://vapp.m-apps.co/api/v1/";
    public Transform comments;
    public Transform commentPrefab;
    public Text comments_count;
    public Text likes_count;
    public Text videoTitle;
    public Image cover;
    public VideoPlayer video;
    public GameObject loadingIndicator;
    private void Start()
    {
    }
    // Use this for initialization
    public void GetVideoDetails() {

        Debug.Log("Clickedddddddddddddddddddddddddddd");
        StartCoroutine(GetVideoComments(int.Parse(name), PlayerPrefs.GetInt("user_id")));
        //StartCoroutine(GetVideoComments(1, 1));
    }
   

    IEnumerator GetVideoComments(int video_id, int user_id)
    {
        loadingIndicator.SetActive(true);

        WWWForm form = new WWWForm();
        form.AddField("video_id", video_id);
        form.AddField("user_id", user_id);

        using (UnityWebRequest www = UnityWebRequest.Post(APILink + "channels/video/details/en", form))
        {
            yield return www.SendWebRequest();
            loadingIndicator.SetActive(false);


            if (www.isNetworkError || www.isHttpError)
            {
                //Debug.Log(www.error);
            }
            else
            {
                //Debug.Log(www.downloadHandler.text);
                VideoObject videoObject = JsonUtility.FromJson<VideoObject>(www.downloadHandler.text);
                //Debug.Log(videoObject.message);
                if (videoObject.status)
                {
                    for(int i=0;i<comments.childCount;i++) {
                        comments.GetChild(i).gameObject.SetActive(false);
                    }
                    //comments
                    videoTitle.text = videoObject.data.title;
                    video.url = videoObject.data.video;
                    StartCoroutine(downloadImage(cover, videoObject.data.image));

                    videoTitle.text = videoObject.data.title;
                    comments_count.text = videoObject.data.comments.Count+" Comments";
                    likes_count.text = videoObject.data.likes_count + " Likes";
                    foreach (Comments c in videoObject.data.comments) { 
                        Transform comment = Instantiate(commentPrefab);
                        comment.GetChild(0).GetComponent<Text>().text = c.full_name; 
                        //Debug.Log(c.user);
                        comment.GetChild(2).GetComponent<Text>().text = c.comment;
                        comment.GetChild(3).name = c.id + "";
                        if (c.liked == 1)
                        {
                            comment.GetChild(3).GetComponent<Toggle>().isOn = true;
                        }
                        else
                        {
                            comment.GetChild(3).GetComponent<Toggle>().isOn = false;
                        }
                        StartCoroutine(downloadImage(comment.GetChild(1).GetChild(0).GetComponent<Image>(), c.profile_image));
                        comment.SetParent(comments);
                    }
                }
                else
                {

                }
            }
        }
    }
    IEnumerator downloadImage(Image m, string linkk)
    {
        loadingIndicator.SetActive(true);

        // Start a download of the given URL
        using (WWW www = new WWW(linkk))
        {
            Debug.Log("Downlooooooodinggggg");
            // Wait for download to complete
            yield return www;
            loadingIndicator.SetActive(false);

            m.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
    }
    

    [System.Serializable]
    public class VideoObject
    {
        public bool status;
        public string message;
        public Video data;
    }
    [System.Serializable]
    public class Video
    {
        public int id;
        public int user_id;
        public int category_id;
        public string title;
        public string description;
        public string image;
        public string video;
        public int views_count;
        public string status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public int likes_count;
        public Category category;
        public List<Comments> comments;

    }
    [System.Serializable]
    public class Category
    {
        public int id;
        public string title;
        public string updated_at;
        public string created_at;
        public string deleted_at;
    }
    [System.Serializable]
    public class Comments
    {
        public int id;
        public string user_id;
        public string video_id;
        public string comment;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public int liked;
        public string profile_image;
        public string full_name;
    }
    public class User
    {
        public int id;
        public string full_name;
        public string email;
        public string profile_image;
        public string ip;
        public string fcm_token;
        public string device_type;
        public string status;
        public string updated_at;
        public string created_at;
        public string deleted_at;

    }
}
