﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Messages : MonoBehaviour {

    public Transform messageElementPrefab;
    public Transform messagesList;
    public Transform freindElementPrefab;
    public Transform freindsList;


    public GameObject messagesPanel;
    public GameObject noMessagesPanel;
    Transform instan = null;
    public GameObject loadingIndicator;
    public string API_LINK = "https://vapp.m-apps.co/api/v1/conversations/all/en";


    void OnEnable()
    {

        //StartCoroutine(GetMessages(PlayerPrefs.GetString("user_id")));
        StartCoroutine(GetMessages("1"));
        StartCoroutine(GetFreinds("1"));
    }
    IEnumerator GetMessages(string user_id)
    {
        loadingIndicator.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("user_id", user_id);

        using (UnityWebRequest www = UnityWebRequest.Post(API_LINK, form))
        {
            yield return www.SendWebRequest();

            loadingIndicator.SetActive(false);

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);


            }
            else
            {
                for (int i = 0; i < messagesList.childCount; i++)
                {
                    Destroy(messagesList.GetChild(i).gameObject);
                }
                //Debug.Log(www.downloadHandler.text);
                UserObject userObject = JsonUtility.FromJson<UserObject>(www.downloadHandler.text);
                List<Data> data = userObject.data;
                if (data.Count > 0)
                {
                    // Debug.Log(allUsers.Count);
                    messagesPanel.SetActive(true);
                    noMessagesPanel.SetActive(false);
                    for (int i = 0; i < data.Count; i++)
                    {
                        instan = Instantiate(messageElementPrefab);
                        instan.name = data[i].id + "";
                        instan.GetChild(0).name = data[i].from_user.id + "";
                        StartCoroutine(downloadImage(instan.GetChild(0).GetChild(0).GetComponent<Image>(), data[i].from_user.profile_image));
                        instan.GetChild(1).GetChild(0).GetComponent<Text>().text = data[i].from_user.full_name;
                        instan.GetChild(1).GetChild(1).GetComponent<Text>().text = data[i].conversation_details[data[i].conversation_details.Count-1].message;
                        

                        instan.SetParent(messagesList);



                    }
                    loadingIndicator.SetActive(false);

                }
                else
                {
                    messagesPanel.SetActive(false);
                    noMessagesPanel.SetActive(true);
                    loadingIndicator.SetActive(false);

                }
                // listener.OnCompleted(www.downloadHandler.text, callerId);
            }
        }
    }


    IEnumerator GetFreinds(string user_id)
    {
        loadingIndicator.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("user_id", user_id);

        using (UnityWebRequest www = UnityWebRequest.Post("https://vapp.m-apps.co/api/v1/conversations/people/friends/en", form))
        {
            yield return www.SendWebRequest();

            loadingIndicator.SetActive(false);

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);


            }
            else
            {
                for (int i = 1; i < freindsList.childCount; i++)
                {
                    Destroy(freindsList.GetChild(i).gameObject);
                }
                //Debug.Log(www.downloadHandler.text);
                UserObject2 userObject = JsonUtility.FromJson<UserObject2>(www.downloadHandler.text);
                List<User> allUsers = userObject.data;
               
                   
                    for (int i = 0; i < allUsers.Count; i++)
                    {
                        instan = Instantiate(freindElementPrefab);
                        instan.name = allUsers[i].id + "";
                        StartCoroutine(downloadImage(instan.GetChild(0).GetComponent<Image>(), allUsers[i].profile_image));
                      

                        instan.SetParent(freindsList);



                    }

                
            }
        }
    }


    IEnumerator downloadImage(Image m, string linkk)
    {
        loadingIndicator.SetActive(true);

        // Start a download of the given URL
        using (WWW www = new WWW(linkk))
        {
            loadingIndicator.SetActive(false);
            // Wait for download to complete
            yield return www;

            m.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
    }
    [System.Serializable]
    public class User
    {
        public int id;
        public string full_name;
        public string email;
        public string profile_image;
        public string ip;
        public int liked;
        public string fcm_token;
        public string device_type;
        public bool status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        

    }
    [System.Serializable]
    public class UserObject2
    {
        public bool status;
        public string message;
        public List<User> data;
    }

    [System.Serializable]
    public class UserObject
    {
        public bool status;
        public string message;
        public List<Data> data;
    }
    [System.Serializable]
    public class Data
    {
        public int id;
        public User from_user;
        public User to_user;
        public List<ConversationDetails> conversation_details;
    }
    [System.Serializable]
    public class ConversationDetails
    {

        public int from_userconversation_id;
        public int from_user_id;
        public string message_type;
        public string message;
        public string updated_at;
    }


}