﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class channelView : MonoBehaviour {
    public Transform rowPrefab;
    public Transform videoPrefab;
    public Transform vediosList;
    public GameObject videosPanel;
    public GameObject noVideosPanel;
    Transform instan = null;
    public GameObject loadingIndicator;

    //private void Start()
    //{
    //    loadingIndicator.SetActive(true);
    //    StartCoroutine(LoginUser(PlayerPrefs.GetString("username"), PlayerPrefs.GetString("password")));
        
    //    // StartCoroutine(downloadImage());

    //}
    void OnEnable() {
        
        StartCoroutine(LoginUser(PlayerPrefs.GetString("username"), PlayerPrefs.GetString("password")));
    }

     IEnumerator LoginUser(string username, string password)
    {
        loadingIndicator.SetActive(true);

        PlayerPrefs.SetString("username", username);
        PlayerPrefs.SetString("password", password);
        WWWForm form = new WWWForm();
        form.AddField("email", username);
        form.AddField("password", password);

        using (UnityWebRequest www = UnityWebRequest.Post("https://vapp.m-apps.co/api/v1/auth/Login/en", form))
        {
            yield return www.SendWebRequest();

            loadingIndicator.SetActive(false);

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                

            }
            else
            {
                for (int i=1;i< vediosList.childCount;i++) {
                    vediosList.GetChild(i).gameObject.SetActive(false);
                }
                //Debug.Log(www.downloadHandler.text);
                UserObject userObject = JsonUtility.FromJson<UserObject>(www.downloadHandler.text);
                List<Video> allVideos = userObject.data.channel;
                if (allVideos.Count > 0)
                {
                    videosPanel.SetActive(true);
                    noVideosPanel.SetActive(false);
                    for (int i = 0; i < allVideos.Count; i++)
                    {

                        if (i % 2 == 0)
                        {
                            instan = Instantiate(rowPrefab);
                            instan.SetParent(vediosList);
                        }
                        Transform vid = Instantiate(videoPrefab);
                        vid.name = allVideos[i].id + "";
                        vid.GetChild(1).GetComponent<Text>().text = allVideos[i].title;
                        StartCoroutine(downloadImage(vid.GetChild(0).GetComponent<Image>(), allVideos[i].image));
                        vid.SetParent(instan);

                    }
                    loadingIndicator.SetActive(false);

                }
                else
                {
                    videosPanel.SetActive(false);
                    noVideosPanel.SetActive(true);
                    loadingIndicator.SetActive(false);

                }
                // listener.OnCompleted(www.downloadHandler.text, callerId);
            }
        }
    }
    IEnumerator downloadImage(Image m,string linkk)
    {
        loadingIndicator.SetActive(true);

        // Start a download of the given URL
        using (WWW www = new WWW(linkk))
        {
            loadingIndicator.SetActive(false);
            // Wait for download to complete
            yield return www;

            m.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
    }
    [System.Serializable]
    public class User
    {
        public int id;
        public string full_name;
        public string email;
        public string profile_image;
        public string ip;
        public string fcm_token;
        public string device_type;
        public bool status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public List<Video> channel;

    }
    [System.Serializable]
    public class UserObject
    {
        public bool status;
        public string message;
        public User data;
    }
    [System.Serializable]
    public class Video
    {
        public int id;
        public int user_id;
        public int category_id;
        public string title;
        public string description;
        public string image;
        public string video;
        public int views_count;
        public string status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public Category category;

    }
    [System.Serializable]
    public class Category
    {
        public int id;
        public string title;
        public string updated_at;
        public string created_at;
        public string deleted_at;
    }
}
