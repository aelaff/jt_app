﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class UpdateProfile : MonoBehaviour {
    string APILink = "https://vapp.m-apps.co/api/v1/auth/";
    public InputField full_name_text;
    public InputField email_text;
    public InputField password_text;
    public Image profile_image;
    public GameObject loadingIndicator;

    public void Start()
    {
        full_name_text.text = API.userObject.data.full_name;
        email_text.text = API.userObject.data.email;
        StartCoroutine(downloadImage(profile_image, API.userObject.data.profile_image));
    }
    public void UpdateProf()
    {
        StartCoroutine(UpdatePro(full_name_text.text, email_text.text, password_text.text, "", PlayerPrefs.GetInt("user_id")));
    }
    IEnumerator UpdatePro(string full_name,string email,string password,string profile_image, int user_id)
    {

        WWWForm form = new WWWForm();
        form.AddField("full_name", full_name);
        form.AddField("email", email);
        form.AddField("password", password);
        form.AddField("profile_image", profile_image);
        form.AddField("user_id", user_id);
        loadingIndicator.SetActive(true);

        using (UnityWebRequest www = UnityWebRequest.Post(APILink + "updateProfile/en", form))
        {
            yield return www.SendWebRequest();
            loadingIndicator.SetActive(false);


            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                //listener.OnFailed(callerId);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                UserObject userObj = JsonUtility.FromJson<UserObject>(www.downloadHandler.text);
                //API.userObject = userObj as API.UserObject;
                Debug.Log(userObj.message);
                if (userObj.status)
                {
                    
                }
                else
                {

                }
            }
        }
    }
    IEnumerator downloadImage(Image m, string linkk)
    {
        loadingIndicator.SetActive(true);

        // Start a download of the given URL
        using (WWW www = new WWW(linkk))
        {
            Debug.Log("Downlooooooodinggggg");
            // Wait for download to complete
            yield return www;
            loadingIndicator.SetActive(false);

            m.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
    }

    
    [System.Serializable]
    public class User
    {
        public int id;
        public string full_name;
        public string email;
        public string profile_image;
        public string ip;
        public string fcm_token;
        public string device_type;
        public bool status;
        public string updated_at;
        public string created_at;
        public string deleted_at;

    }
    [System.Serializable]
    public class UserObject
    {
        public bool status;
        public string message;
        public User data;
    }
}
