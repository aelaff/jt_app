﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class FreindsList : MonoBehaviour {
    public Transform rowPrefab;
    public Transform freindElementPrefab;
    public Transform freindsList;
    public GameObject freindsPanel;
    public GameObject noFreindsPanel;
    Transform instan = null;
    public GameObject loadingIndicator;
    public string API_LINK = "https://vapp.m-apps.co/api/v1/conversations/people/friends/en";


    void OnEnable() {
        
        //StartCoroutine(GetFreinds(PlayerPrefs.GetString("user_id")));
        StartCoroutine(GetFreinds("1"));
    }
    IEnumerator GetFreinds(string user_id)
    {
        loadingIndicator.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("user_id", user_id);

        using (UnityWebRequest www = UnityWebRequest.Post(API_LINK, form))
        {
            yield return www.SendWebRequest();

            loadingIndicator.SetActive(false);

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);


            }
            else
            {
                for (int i = 0; i < freindsList.childCount; i++)
                {
                    Destroy(freindsList.GetChild(i).gameObject);
                }
                //Debug.Log(www.downloadHandler.text);
                UserObject userObject = JsonUtility.FromJson<UserObject>(www.downloadHandler.text);
                List<User> allUsers = userObject.data;
                if (allUsers.Count > 0)
                {
                   // Debug.Log(allUsers.Count);
                    freindsPanel.SetActive(true);
                    noFreindsPanel.SetActive(false);
                    for (int i = 0; i < allUsers.Count; i++)
                    {
                        instan = Instantiate(freindElementPrefab);
                        instan.name = allUsers[i].id+"";
                        StartCoroutine(downloadImage(instan.GetChild(0).GetChild(0).GetComponent<Image>(), allUsers[i].profile_image));
                        instan.GetChild(1).GetChild(0).GetComponent<Text>().text = allUsers[i].full_name;
                        instan.GetChild(1).GetChild(1).GetComponent<Text>().text = allUsers[i].email;
                        if (instan.childCount > 2)
                        {
                            if (allUsers[i].liked == 1)
                                instan.GetChild(2).GetComponent<Toggle>().isOn = false;
                            else
                                instan.GetChild(2).GetComponent<Toggle>().isOn = true;

                        }

                        instan.SetParent(freindsList);



                    }
                    loadingIndicator.SetActive(false);

                }
                else
                {
                    freindsPanel.SetActive(false);
                    noFreindsPanel.SetActive(true);
                    loadingIndicator.SetActive(false);

                }
                // listener.OnCompleted(www.downloadHandler.text, callerId);
            }
        }
    }


    


    IEnumerator downloadImage(Image m,string linkk)
    {
        loadingIndicator.SetActive(true);

        // Start a download of the given URL
        using (WWW www = new WWW(linkk))
        {
            loadingIndicator.SetActive(false);
            // Wait for download to complete
            yield return www;

            m.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
    }
    [System.Serializable]
    public class User
    {
        public int id;
        public string full_name;
        public string email;
        public string profile_image;
        public string ip;
        public int liked;
        public string fcm_token;
        public string device_type;
        public bool status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public List<Video> channel;

    }
    [System.Serializable]
    public class UserObject
    {
        public bool status;
        public string message;
        public List<User> data;
    }
    [System.Serializable]
    public class Video
    {
        public int id;
        public int user_id;
        public int category_id;
        public string title;
        public string description;
        public string image;
        public string video;
        public int views_count;
        public string status;
        public string updated_at;
        public string created_at;
        public string deleted_at;
        public Category category;

    }
    [System.Serializable]
    public class Category
    {
        public int id;
        public string title;
        public string updated_at;
        public string created_at;
        public string deleted_at;
    }
}
