﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoEditManager : MonoBehaviour
{
    string APILink = "https://vapp.m-apps.co/api/v1/";

    public VideoPlayer VideoPlayer;
    public Button PlayButton, PauseButton;
    public InputField TitleInputField, DescriptionInputField;
    public string pathToUpload;
    public GameObject LoadingPanal;

    private void Start()
    {

        LoadingPanal.SetActive(false);
    }


    // play video
    public void Play()
    {
        VideoPlayer.Play();
    }


    // video to base64;
    public string ToBase64(string filePath)
    {
        Byte[] bytes = File.ReadAllBytes(filePath);
        return Convert.ToBase64String(bytes);

    }

    public void PickVideo()
    {
        PauseButton.onClick.Invoke();
        NativeGallery.Permission permission = NativeGallery.GetVideoFromGallery((path) =>
        {
            Debug.Log("Video path: " + path);
            if (path != null)
            {
                pathToUpload = path;
                VideoPlayer.url = "file://" + path;
                VideoPlayer.Play();
            }
        }, "Select a video");

        Debug.Log("Permission result: " + permission);
    }


    public void RecordVideo()
    {
        NativeCamera.Permission permission = NativeCamera.RecordVideo((path) =>
        {
            Debug.Log("Video path: " + path);
            if (path != null)
            {

                pathToUpload = path;
                VideoPlayer.url = "file://" + path;
                VideoPlayer.Play();
            }
        });

        Debug.Log("Permission result: " + permission);
    }


    // upload code

    public void UploadVideo()
    {
        LoadingPanal.SetActive(true);
        string videoBase64String = ToBase64(pathToUpload);
        //string videoBase64String = ToBase64("C:/Users/HP/Downloads/a.webm");
        //Debug.Log(videoBase64String);
        //UploadVideoObject videoObject = new UploadVideoObject();
        //videoObject.user_id = 1;
        //videoObject.category_id = 1;
        //videoObject.title = TitleInputField.text.Trim();
        //videoObject.description = DescriptionInputField.text.Trim();
        //videoObject.video = videoBase64String;
        //..StartCoroutine(UploadVidAsync(videoBase64String, 1, 1, TitleInputField.text.Trim(), DescriptionInputField.text.Trim()));
        StartCoroutine(Upload("data:video/mp4;base64," + videoBase64String));
    }

    
    IEnumerator Upload(string video)
    {
        WWWForm form = new WWWForm();
        form.AddField("user_id", PlayerPrefs.GetInt("user_id"));
        form.AddField("category_id", 1);
        form.AddField("title", TitleInputField.text.Trim());
        form.AddField("description", DescriptionInputField.text.Trim());
        form.AddField("video", video);

        UnityWebRequest www = UnityWebRequest.Post("https://vapp.m-apps.co/api/v1/channels/new/video/en", form);
        yield return www.SendWebRequest();
        LoadingPanal.SetActive(false);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Form upload complete!");
        }
    }
    [Serializable]
    public class UploadVideoObject
    {
        public int user_id;
        public int category_id;
        public string title;
        public string description;
        public string video;
    }
}