﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Conversation : MonoBehaviour {
    public Transform meCommentElementPrefab;
    public Transform freindCommentElementPrefab;
    public Transform CommentsList;
    public GameObject ConversationDetailsPanel;
    Transform instan = null;
    public GameObject loadingIndicator;
    public string API_LINK = "https://vapp.m-apps.co/api/v1/conversations/details/en";
    public Image freindImage;
    public Text freindName;
    public void ShowConversation() {
        PlayerPrefs.SetInt("ConvId", int.Parse(name));
        PlayerPrefs.SetInt("FromUser", int.Parse(transform.GetChild(0).name));
        StartCoroutine(showConversation(1,int.Parse(name)));
    }
  

    
    IEnumerator showConversation(int user_id, int conversation_id)
    {
        loadingIndicator.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("user_id", user_id);
        form.AddField("conversation_id", conversation_id);

        using (UnityWebRequest www = UnityWebRequest.Post(API_LINK, form))
        {
            yield return www.SendWebRequest();

            loadingIndicator.SetActive(false);

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);


            }
            else
            {
                for (int i = 0; i < CommentsList.childCount; i++)
                {
                    Destroy(CommentsList.GetChild(i).gameObject);
                }
                //Debug.Log(www.downloadHandler.text);
                UserObject userObject = JsonUtility.FromJson<UserObject>(www.downloadHandler.text);
                List<ConversationDetails> conversation_details = userObject.data.conversation_details;
                Data data = userObject.data;

                // Debug.Log(allUsers.Count);

                for (int i = 0; i < conversation_details.Count; i++)
                    {
                    StartCoroutine(downloadImage(freindImage, data.from_user.profile_image));
                    freindName.GetComponent<Text>().text = data.from_user.full_name;
                    if (user_id == conversation_details[i].from_user_id) { 
                        instan = Instantiate(meCommentElementPrefab);
                    }
                    else { 
                        instan = Instantiate(freindCommentElementPrefab);
                       
                    }

                    

                    instan.GetChild(0).GetChild(0).GetComponent<Text>().text = conversation_details[i].message;
                     

                        instan.SetParent(CommentsList);



                    }
                    loadingIndicator.SetActive(false);

                
            }
        }
    }





    IEnumerator downloadImage(Image m, string linkk)
    {
        loadingIndicator.SetActive(true);

        // Start a download of the given URL
        using (WWW www = new WWW(linkk))
        {
            loadingIndicator.SetActive(false);
            // Wait for download to complete
            yield return www;

            m.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
        ConversationDetailsPanel.SetActive(true);

    }
    [System.Serializable]
    public class User
    {
        public int id;
        public string full_name;
        public string email;
        public string profile_image;
        public string ip;
        public int liked;
        public string fcm_token;
        public string device_type;
        public bool status;
        public string updated_at;
        public string created_at;
        public string deleted_at;


    }
    [System.Serializable]
    public class UserObject2
    {
        public bool status;
        public string message;
        public List<User> data;
    }

    [System.Serializable]
    public class UserObject
    {
        public bool status;
        public string message;
        public Data data;
    }
    [System.Serializable]
    public class Data
    {
        public int id;
        public User from_user;
        public User to_user;
        public List<ConversationDetails> conversation_details;
    }
    [System.Serializable]
    public class ConversationDetails
    {

        public int from_userconversation_id;
        public int from_user_id;
        public string message_type;
        public string message;
        public string updated_at;
    }
}
